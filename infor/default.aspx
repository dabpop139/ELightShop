﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="about_default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>用户新窗口IDC-关于我们</title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<form id="xform" runat="server">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="SimpageDiv">
    <div class="MLeft">
	    <div class="LDiv_1">
		    <ul class="Ti_1">
			    <li class="i1"><h3><a href="/infor/">行业资讯</a></h3></li>
			    <li class="i2"><a href="/infor/">更多&gt;&gt;</a></li>
			    <li class="mclear"></li>
		    </ul>
		    <div class="DList">
			    <ul>
				    <asp:Literal id="Ft_News" runat="server"></asp:Literal>
			    </ul>
		    </div>
	    </div><!--LDiv_1-->
	    <div class="LDiv_M">
		    <ul class="Ti_1">
			    <li class="i1"><h3>快速导航</h3></li>
			    <li class="i2"></li>
			    <li class="mclear"></li>
		    </ul>
		    <div class="Menu">
			    <ul>				    
				    <li><a href="/contact/">联系我们</a></li>
                    <li><a href="/infor/?cate=2">动态公告</a></li>
                    <li><a href="/infor/">行业新闻</a></li>
                    <li><a href="/price/">价格总览</a></li>
                    <li><a href="/pay/">在线充值</a></li>
			    </ul>
		    </div>
	    </div>
	    <div class="LDiv_P"><a href="/pay/" target="_blank"><img alt="" src="/stylecss/images/tip_1.gif" /></a></div>
	    <div class="LDiv_L">
		    <ul class="Ti_1">
			    <li class="i1"><h3><a href="/help/">导购指南</a></h3></li>
			    <li class="i2"><a href="/help/">更多&gt;&gt;</a></li>
			    <li class="mclear"></li>
		    </ul>
		    <div class="DList">
			    <ul>
				    <asp:Literal id="Ft_Help" runat="server"></asp:Literal>
			    </ul>
		    </div>
	    </div><!--LDiv_4-->
	    <div class="LDiv_5">
		    <ul>
				<li class="i1"><a href="<%=Si.email%>" target="_blank" title="电子邮箱">电子邮箱</a></li>
				<li class="i2"><a href="<%=Si.msnonline%>" target="_blank" title="MSN在线">MSN在线</a></li>
				<li class="i3"><a href="<%=Si.qqonline%>" target="_blank" title="QQ在线">QQ在线</a></li>
			    <li class="mclear"></li>
		    </ul>
	    </div>
    </div><!--MLeft-->
    <div class="MRight">
    	<div class="Elem_1"><img src="/stylecss/images/show_06.jpg" /></div>
        <div class="Elem_2">
			<ul class="Ti_1">
				<li class="i1"><h3><a href="/about/">关于我们</a></h3></li>
				<li class="i2"></li>
				<li class="mclear"></li>
			</ul>
            <div class="CoData">
                    <table width="96%" border="1" align="center" bordercolor="#cccccc" cellpadding="0" cellspacing="0" class="btable">
                      <tbody>
                        <tr>
                          <td align="center">
                            <h2>用户新窗口IDC 简介</h2>
                          </td>
                        </tr>
                        <tr>
                            <td>
                                <P>　　用户新窗口IDC (idc.uxwin.com) 提供专业的企业网站设计、门户网站设计开发、网站维护、网站推广、网络系统开发、网站主机、网站空间、网站虚拟空间等一系列的互联网应用服务。</P>
                                <P>　　本团队由多名资深的网页设计师与网站开发程序员组成，拥有强硬的技术力量； 根据不同的客户需要进行创意的设计，量身定制专业精美的网站；根据客户的行业特色对该网站进来维护及相应的营销推广指导！</P>
                                <P>　　<b>我们的优势</b></P>
                                <P>　　强硬的技术力量： 用户新窗口IDC不仅有雄厚的网络技术实力，还有一批资深的专业设计师，真正为企业设计开始既有实用性的网站，真正达到企业上网的真正要求与目的。 设计实用性的网站： 根据企业的需要和潜在市场需求设计出企业满意的网站。</P>
                                <P>　　<b>选择我们的理由</b></P>
                                <P>　　及时的沟通交流： 良好的沟通将贯穿在整个开发过程的始终，我们将及时了解您的需求结合我们专业的技术设计出适合企业文化的专业网站。</P>
                                <P>　　优质的后期服务： 根据企业需要，在未来一年内在不改变整体结构的情况下为企业维护网站。 </P>
                                <P>　　实惠的价格： 良好的策划，高效率的开发，为您大大节省网站成本开支；我们坚持最优惠价格，为客户节省大量制作费用；</P>
                            </td>
                        </tr>
                      </tbody>
                    </table>
            </div>
		</div><!--Elem_2-->
    </div><!--MRight-->
    <p class="mclear"></p>
</div><!--HostDiv-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</form>
</body>
</html>
