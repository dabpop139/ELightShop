﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="my-profile.aspx.cs" Inherits="account_my_profile" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
<script language="javascript" type="text/javascript">
var iKits={
	light_alert:function(tips){var obj01,obj02;if($3("#light_alert div.txt:0")){obj01=$3("#light_alert");obj02=$3("#light_fade")}else{var objN=document.createElement("div");var htmls="<div class=\"txt\">!</div>\n<div class=\"opera\"><span class=\"btn\" onclick=\"iKits.light_alert_close();\">CONFIRM</span></div>";objN.innerHTML=htmls;objN.setAttribute("id","light_alert");$3.box().appendChild(objN);$3("#light_alert div.txt:0").innerHTML=tips;var objM=document.createElement("div");objM.setAttribute("id","light_fade");$3.box().appendChild(objM);obj01=objN;obj02=objM;objN=objM=null}if(obj01!=undefined){obj01.style.display="block";obj01.style.left=parseInt(($3.box().clientWidth-obj01.clientWidth-18)/2)+"px";obj01.style.top=parseInt($3.elem().scrollTop+($3.elem().clientHeight/2)-(obj01.clientHeight/2)-18)+"px";obj02.style.display="block";obj02.style.width=$3.box().clientWidth+"px";obj02.style.height=$3.box().scrollHeight+"px";$3("#light_alert div.txt:0").innerHTML=tips;obj01=obj02=null}},
	light_alert_close:function(){$3("#light_alert").style.display="none";$3("#light_fade").style.display="none";$3("#light_fade").style.width=$3("#light_fade").style.height="5px"}
};
function change_date() {
    var monthctrl = $3("#month").value;
    var datectrl = $3("#days");
    datectrl.innerHTML="";
    var i = 0;
    switch (monthctrl) {
    case "1":
    case "3":
    case "5":
    case "7":
    case "8":
    case "10":
    case "12":
        i = 31;
        break;
    case "2":
        var year = $3("#years").value;
        if (year != "" && year % 4 == 0 && year != 0) {
            i = 29;
        } else {
            i = 28;
        }
        break;
    case "4":
    case "6":
    case "9":
    case "11":
        i = 30;
        break;
    case "":
        i = 0;
        break;
    }
    datectrl.appendChild(new Option("Day"," "));
    if (i > 0) {
        for (var j = 1; j <= i; j++) {
            datectrl.appendChild(new Option(j,j));
        }
    }
}
function change_year() {
    var year = $3("#years").value;
    var month = $3("#month").value;
    if (month == "2") {
        if (year % 4 == 0) {
            if ($3("#days").options.length != 30) {
                $3("#days").appendChild(new Option("29","29"));
            }
        } else {
            if ($3("#days").options.length == 30) {
               $3("#days").removeChild($3("#days").options[$3("#days").options.length-1]);
            }
        }
    }
}
function showtab(n){
	$3("#profile_box div.profile").each(function(){
		$3(this).style.display="none";
	});
	$3("#profile_box div.itab_st01 a").each(function(){
		$3(this).className="nor";
	});
	
	for(var i=0; i<document.forms[0].elements.length; i++){
	    with(document.forms[0].elements[i]){
	        if(typeof(getAttribute("datatype")) == "object") continue;
	        setAttribute("datatype","dis_"+getAttribute("datatype").replace("dis_",""));
	        if(getAttribute("id").indexOf("act"+n+"_")!=-1){
	            setAttribute("datatype",getAttribute("datatype").replace("dis_",""));
	        }
	    }
	}
	
	$3("#profile_box div.profile:"+n).style.display="block";
	$3("#profile_box div.profile:"+n+"")
	$3("#entry").value="act_0"+n;
	//$3.evtsrc().className="cur";
	$3("#profile_box div.itab_st01 a:"+n).className="cur";
	
}
function sele_current(){
    var nCurr=Number($3.request("current").replace("act",""));
    showtab(nCurr);
}
$3.onload(function(){
    sele_current();
});
</script>
</head>
<body>
<form id="xform" runat="server" action="my-profile.aspx" onsubmit="return validator.validate(this,4);">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="UserCen">
	<div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">Account</a></div>
	<Stencil:AccLeftTemp id="AccLeftTemp_" runat="server" />
    <div class="RightDiv">
        <div class="acc_box">
<div class="profile_box" id="profile_box">
	<div class="itab_st01">
    	<h3><a href="javascript:void(0)" onclick="showtab(0);" class="nor">Basic information</a><a href="javascript:void(0)" onclick="showtab(1);" class="nor">Change your Email address</a><a href="javascript:void(0)" onclick="showtab(2);" class="nor">Change your password</a></h3>
    </div>
	<div class="profile_vessel">
	    <div><input type="hidden" id="entry" value="" runat="server" /><input id="nickname" type="hidden" value="" runat="server" /></div>
        <div class="profile" style="display:none;">
        <table border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td width="20%" align="right"><span class="labels">Email Address:</span></td>
              <td><span class="subs"><asp:Literal id="Ft_CurrEmail01" runat="server"/></span></td>
            </tr>
            <tr>
              <td align="right"><span class="labels">NickName:</span></td>
              <td><span class="subs"><asp:Literal id="Ft_NickName" runat="server"/></span></td>
            </tr>
            <tr>
              <td align="right"><span class="labels"><label class="ierror"></label>First Name:</span></td>
              <td><input type="text" maxlength="20" value="" id="act0_firstname" class="inputs" msg="" datatype="require" runat="server" /></td>
            </tr>
            <tr>
              <td align="right"><span class="labels"><label class="ierror"></label>Last Name:</span></td>
              <td><input type="text" maxlength="29" value="" id="act0_lastname" class="inputs" msg="" datatype="require" runat="server" /></td>
            </tr>
            <tr>
              <td align="right"><span class="labels">Gender:</span></td>
              <td>
                <input type="radio" id="act0_gender01" name="act0_gender" class="radio_er" runat="server" checked="true" />
                Male
                <input type="radio" id="act0_gender02" name="act0_gender" class="radio_er" runat="server" />
                Female
              </td>
            </tr>
            <tr>
              <td align="right" valign="top"><span class="labels"><label class="ierror"></label>Birthday:</span></td>
              <td>
              	<select id="act0_years" class="selects" onchange="change_year();" msg="" datatype="require" runat="server">
                  <option value="">Year</option>
                </select>
                <select id="act0_month" class="selects" onchange="change_date();" msg="" datatype="require" runat="server">
                  <option value="">Month </option>
                </select>
                <select id="act0_days" class="selects" msg="" datatype="require" runat="server">
                  <option value="">Day</option>
                </select>
                <div class="tips" style="width: 290px; line-height: 15px;">we will not display your date of birth, but we will send you a surprise birthday gift then.</div></td>
            </tr>
            <tr>
              <td align="right"><span class="labels"><label class="ierror"></label>Country:</span></td>
              <td><select id="act0_country" class="selects" msg="" datatype="require" runat="server" style="width: 192px;">
                  <option value="">Please select</option>
                </select>
              </td>
            </tr>
            <tr>
              <td align="right"><span class="labels">Marital Status:</span></td>
              <td>
                <input type="radio" id="act0_marital01" name="act0_marital" class="radio_er" runat="server" />
                Single
                <input type="radio" id="act0_marital02" name="act0_marital" class="radio_er" runat="server" />
                Married
                <input type="radio" id="act0_marital03" name="act0_marital" class="radio_er" runat="server" />
                Rather not say
              </td>
            </tr>
            <tr>
              <td align="right" valign="top"><span class="labels">About Me:</span></td>
              <td><textarea id="act0_remark" cols="0" rows="0" style="width: 335px; height: 115px; border: 1px solid #dddddd;" runat="server"></textarea></td>
            </tr>
            <tr>
              <td align="right"></td>
              <td><div class="mt10"><input type="submit" value="Save Changes" class="inputs_btn_st01" /></div></td>
            </tr>
          </tbody>
        </table>
        </div><!--profile_01-->
        <div class="profile" style="display:none;">
            <table border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td width="32%" align="right"><span class="labels"><label class="ierror"></label>Current Email Address:</span></td>
                  <td><span class="subs"><asp:Literal id="Ft_CurrEmail02" runat="server"/></span></td>
                </tr>
                <tr>
                  <td align="right"><span class="labels"><label class="ierror"></label>New Email Address:</span></td>
                  <td><input type="text" maxlength="50" value="" class="inputs" id="act1_newemail" msg="" datatype="require" runat="server"/>
                  </td>
                </tr>
                <tr>
                  <td align="right"><span class="labels"><label class="ierror"></label>Re-enter New Email Address:</span></td>
                  <td><input type="text" maxlength="50" value="" class="inputs" id="act1_reemail" msg="" datatype="repeat" to="act1_newemail" runat="server"/></td>
                </tr>
                <%--<tr>
                  <td align="right"><span class="labels"><label class="ierror"></label>Word Verification:</span></td>
                  <td>
                  <input type="text" style="width:90px;" value="" class="inputs" id="act1_validate01"/>
                  <img src="/signin/validcode" onclick="this.src=this.src+'?rnd='+Math.random()" alt=""/></td>
                </tr>--%>
                <tr>
                  <td></td>
                  <td><div class="mt10"><input type="submit" value="Save Changes" class="inputs_btn_st01" /></div></td>
                </tr>
              </tbody>
            </table>

        </div><!--profile_02-->
        <div class="profile" style="display:none;">
            <table border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td width="25%" align="right">Email Address:</td>
                  <td><span class="subs"><asp:Literal id="Ft_CurrEmail03" runat="server"/></span></span></td>
                </tr>
                <tr>
                  <td align="right"><span class="labels">
                    <label class="ierror"></label>Old Password:</span></td>
                  <td><input type="password" maxlength="20" value="" class="inputs" id="act2_oldpassword" msg="" datatype="require" runat="server"/>
                 </td>
                </tr>
                <tr>
                  <td align="right"><span class="labels">
                    <label class="ierror"></label>New Password:</span></td>
                  <td><input type="password" maxlength="20" value="" class="inputs" id="act2_newpassword" msg="" datatype="require" runat="server"/>
                 </td>
                </tr>
                <tr>
                  <td align="right"><span class="labels">
                    <label class="ierror"></label>Confirm Password:</span></td>
                  <td><input type="password" maxlength="20" value="" class="inputs" id="act2_confirmpassword" msg="" datatype="repeat" to="act2_newpassword" runat="server"/>
                 </td>
                </tr>
                <%--<tr>
                  <td align="right"><span class="labels"><label class="ierror"></label>Word Verification:</span></td>
                  <td>
                  <input type="text" style="width:90px;" value="" class="inputs" id="act2_validate02"/>
                  <img src="/signin/validcode" onclick="this.src=this.src+'?rnd='+Math.random()" alt=""/>
                  </td>
                </tr>--%>
                <tr>
                  <td align="right"></td>
                  <td><div class="mt10"><input type="submit" value="Save Changes" class="inputs_btn_st01" /></div></td>
                </tr>
              </tbody>
            </table>
        </div><!--profile_03-->
	</div>
</div>
        </div>
    </div><!--RightDiv-->
    <p class="mclear"></p>
    
</div><!--UserCen-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
<asp:Literal id="Ft_Lalert" runat="server"/>
</form>
</body>
</html>