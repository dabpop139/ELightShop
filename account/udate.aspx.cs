﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using YduCLB.SQL;

public partial class account_udate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();

        int nId = Convert.ToInt16(Request.QueryString["uid"]);
        string sAct = Request.QueryString["act"];
        string sStand = Request.QueryString["stand"];

        if (nId != 0 || sAct != "")
        {
            if (sAct == "wishlist-del!rec" && Session["Member"] != null)
            {
                try
                {
                    if (sStand == "del")
                    {
                        SqlDataReader objUpData_001 = Tc.ConnDate("update " + TableName.db_wishlist + " set delmark=1 where id=" + nId + " and memid=" + Session["MemberID"].ToString());
                        objUpData_001.Close();
                        objUpData_001.Dispose();
                    }
                    if (sStand == "rec")
                    {
                        SqlDataReader objUpData_002 = Tc.ConnDate("update " + TableName.db_wishlist + " set delmark=0 where id=" + nId + " and memid=" + Session["MemberID"].ToString());
                        objUpData_002.Close();
                        objUpData_002.Dispose();
                    }
                }
                catch
                {

                }
            }
        }

        Tc.CloseConn();
        Response.Redirect("/stylecss/images/blank.gif");
    }
}
