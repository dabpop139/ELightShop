﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="product_param.aspx.cs" Inherits="_manage_product_param" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=MainPName%>管理中心</title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Include/Validator.js"></script>
</head>
<body>
<form id="xform" name="xform" runat="server" action="" method="post" onSubmit="return Validator.Validate(this,3);">
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram_Position">
  <tr>
    <td>您现在的位置：<a href="#"><%=MainPName%>管理中心</a> &gt;&gt; <a href="#"><%=SecPName%></a></td>
  </tr>
</table>
<table width="40%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram">
  <tr>
    <td align="center"><h2><%=MainPName%>管理中心----<%=SecPName%></h2></td>
  </tr>
</table>
  <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1">
    <tbody>
      <tr>
        <td class="BarStyleV1"><h3>管理 </h3></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="BarStyleV1">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1" style="margin-top: 0px; border: none;">
                <tbody>
                    <asp:Repeater id="Ft_List" runat="server">
                    <ItemTemplate>
                    <tr>
                        <td width="22%" align="right" valign="middle"><%#Eval("pname")%>：</td>
                        <td align="left" valign="middle">
                        <input id="<%#Eval("pname")%>_txt" name="<%#Eval("pname")%>_txt" size="35" maxlength="60" value="<%#Eval("pvalue")%>" msg="" datatype="Require" runat="server" />
                        </td>
                    </tr>
                    </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td width="22%" align="right" valign="middle">　</td>
                        <td>
                            <input type="submit" value="提交" name="Submit" class="Btn_v1" id="Submit" />
                            <input id="action" type="hidden" value="ctrl_add" name="action" runat="server" />
                            <input id="actid" type="hidden" value="0" name="actid" runat="server" />
                            <input type="reset" value="重置" name="reset" class="Btn_v1" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
      </tr>
    </tbody>
  </table>
</form>
</body>
</html>
