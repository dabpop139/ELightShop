﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Data;
using YduCLB.Func;

public partial class _manage_product_ctrl : System.Web.UI.Page
{
    public string EventName = "product";
    public string MainPName = "产品";
    public string SecPName = "添加产品";
    public string UpFileUrl = "co_pros/";
    Conn Tc = new Conn("level0");
    DbCtrl Dc = new DbCtrl();


    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();

        string nCate = Request.QueryString["cate"];

        PhotoUpload.Attributes["src"] = "upload/photoupload.aspx?pic=img&intent=" + UpFileUrl;
        SqlDataReader drReader = Tc.ConnDate("select * from " + TableName.db_procate + " where id=" + nCate + " order by nord desc");
        while (drReader.Read())
        {
            ListItem ltItem = new ListItem();
            ltItem.Value = drReader["id"].ToString();
            ltItem.Text = drReader["cate_name"].ToString();
            tid.Items.Add(ltItem);
			tid_disc.Value = drReader["cate_disc"].ToString();
        }
        editer_cid.Value = "1018";
        drReader.Dispose();

        int nCtrl = Convert.ToInt32(Request.QueryString["act"]);
        int nId = Convert.ToInt32(Request.QueryString["id"]);

        if (nCtrl == 1)
        {
            SecPName = "编辑产品";
            action.Value = "ctrl_edit";

            SqlDataReader drReaderEd = Tc.ConnDate("select * from " + TableName.db_product + " where id=" + nId);
            while (drReaderEd.Read())
            {
                actid.Value = drReaderEd["id"].ToString();
                titler.Value = drReaderEd["title"].ToString().Replace("&quot;", "\"");
				tid_disc.Value = drReaderEd["cate_disc"].ToString();
                was.Value = drReaderEd["was"].ToString();
                price.Value = drReaderEd["price"].ToString();
                editer.Value = drReaderEd["editor"].ToString();
                editer_cid.Value = drReaderEd["editor_id"].ToString();
                img.Value = drReaderEd["small_pic"].ToString();
                pview_img.Value = drReaderEd["preview_pic"].ToString();
                siminfo.Value = drReaderEd["brief"].ToString();
                editor_overview.Value = drReaderEd["overview"].ToString();
                editor_details.Value = drReaderEd["details"].ToString();
                keywords.Value = drReaderEd["keychar"].ToString();

                int nNum = 0;
                foreach (ListItem item in tid.Items)
                {
                    if (item.Value == drReaderEd["cateid"].ToString())
                    {
                        tid.SelectedIndex = nNum;
                    }
                    nNum++;
                }
                ck_box_1.Checked = drReaderEd["is_top"].ToString() == "1" ? true : false;
                ck_box_2.Checked = drReaderEd["is_rec"].ToString() == "1" ? true : false;
                ck_box_3.Checked = drReaderEd["is_new"].ToString() == "1" ? true : false;
                ck_box_4.Checked = drReaderEd["is_hot"].ToString() == "1" ? true : false;
                ck_box_5.Checked = drReaderEd["is_chk"].ToString() == "1" ? true : false;
                freeshipck.Checked = drReaderEd["fship"].ToString() == "1" ? true : false;
            }
            drReaderEd.Dispose();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region
        if (IsPostBack)
        {
            Models Md = new Models();
            Other Ot = new Other();
            string appdir = string.Empty;
            appdir = Ot.GetAppSetValue("AppDir");

            string actid_ = actid.Value;
            string titler_ = titler.Value.Replace("\"", "&quot;");
            string tid_ = tid.Value;
			string tid_disc_ = tid_disc.Value;
            string was_ = was.Value;
            string price_ = price.Value;
            string editer_ = editer.Value;
            string editer_cid_ = editer_cid.Value;
            string img_ = img.Value;
            string pview_img_ = pview_img.Value;
            string siminfo_ = siminfo.Value;
            string editor_overview_ = editor_overview.Value;
            string editor_details_ = editor_details.Value;
            if (Setting.EnSaveRemoteImg)
            {
                pview_img_ = Ot.SaveRemoteImg(pview_img_, UpFileUrl);
                editor_overview_ = Ot.SaveRemoteImg(editor_overview_, UpFileUrl);
                editor_details_ = Ot.SaveRemoteImg(editor_details_, UpFileUrl);
            }
            
            string keys_ = keywords.Value;
            keys_ = Md.DeaKeyWord(keys_);

            img_ = (img_ == "") ? "noimages.gif" : img_;
            pview_img_ = (pview_img_ == "") ? "noimages.gif" : pview_img_;

            img_ = img_.Replace(appdir, "/");
            //if (img_ != "noimages.gif")
            //{
            //    Ot.LimitImgW(img_, 300, "W");
            //}
            string is_top_ = (ck_box_1.Checked == true) ? "1" : "0";
            string is_rec_ = (ck_box_2.Checked == true) ? "1" : "0";
            string is_new_ = (ck_box_3.Checked == true) ? "1" : "0";
            string is_hot_ = (ck_box_4.Checked == true) ? "1" : "0";
            string is_chk_ = (ck_box_5.Checked == true) ? "1" : "0";
            string freeshipck_ = (freeshipck.Checked == true) ? "1" : "0";

            string[] fieldval = new string[]{"title:"+titler_,
                                                    "cateid:"+tid_,
                                                    "was:"+was_,
                                                    "price:"+price_,
                                                    "fship:"+freeshipck_,
                                                    "brief:"+siminfo_,
                                                    "overview:"+editor_overview_,
                                                    "details:"+editor_details_,
                                                    "small_pic:"+img_,
                                                    "preview_pic:"+pview_img_,
                                                    "keychar:"+keys_,
                                                    "is_top:"+is_top_,
                                                    "is_rec:"+is_rec_,
                                                    "is_new:"+is_new_,
                                                    "is_hot:"+is_hot_,
                                                    "is_chk:"+is_chk_,
                                                    "editor_id:"+editer_cid_,
                                                    "editor:"+editer_,
													"cate_disc:"+tid_disc_};
            int nResult = 1;
            if (action.Value == "ctrl_add")
            {
                nResult = Dc.AddRecord(TableName.db_product, fieldval);
                if (nResult > 0)
                {
                    Other.net_alert("操作出错！");
                }
                else
                {
                    Other.net_alert("操作成功！", EventName + "_manage.aspx");
                }
            }
            if (action.Value == "ctrl_edit")
            {
                nResult = Dc.UpdateRecord(TableName.db_product, "id=" + actid_, fieldval);
                if (nResult > 0)
                {
                    Other.net_alert("操作出错！");
                }
                else
                {
                    Other.net_alert("操作成功！", "close");
                }
            }


        }
        #endregion
		Tc.CloseConn();
    }
}
