var $ = function(id){return "string"==typeof id ? document.getElementById(id) : id;};
var URLParams = new Object();
var isIE=isMSIE();
var oMyEditor,oCtrlWin;
var sPath="";
var aParams = window.location.search.substr(1).split("&") ;
for (i=0 ; i < aParams.length ; i++) {
	var aParam = aParams[i].split("=") ;
	URLParams[aParam[0]] = aParam[1] ;
}

function isMSIE(){
    if(typeof(document.all)=="object"){
        return true;
    }else{
        return false;
    }
}

if(URLParams["iframe"]){
	var oIframe=window.parent.document.getElementById(URLParams["iframe"]);
	if(oIframe){
		/*var shref = window.location.href;
		var sloca = shref.split("/");
		sPath=shref.replace(sloca[sloca.length-1],"");
		sPath=sPath.replace(sloca[sloca.length-2]+"/","");*/
		oMyEditor = oIframe.contentWindow.MyIEL_Editor;
		oCtrlWin = oIframe.contentWindow.objEditor.contentWindow;
	}else{
		alert("无法找到相关对象初始化失败！");
		xClose();
	}
	oIframe = null;
}else{
	oMyEditor = window.parent.MyIEL_Editor;
	oCtrlWin = window.parent.objEditor.contentWindow;
}

function xClose(){
	window.parent.document.getElementById(URLParams["obj"]).src="about:blank";
	window.parent.document.getElementById(URLParams["obj"]).style.display="none";
}

function SearchSelectValue(o_Select, s_Value){
	for(var i=0;i<o_Select.length;i++){
		if (o_Select.options[i].value == s_Value){
			o_Select.selectedIndex = i;
			return true;
		}
	}
	return false;
}

function IsDigit(){
	return ((event.keyCode >= 48) && (event.keyCode <= 57));
}

function BaseAlert(theText,notice){
	alert(notice);
	theText.focus();
	theText.select();
	return false;
}