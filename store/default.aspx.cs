﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;
using YduCLB.Front;

public partial class store_default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        GList xGList = new GList();
        FCtrl xFctrl = new FCtrl();
        Conn Tc = new Conn();

        ArrayList Result = new ArrayList();
        string[] aRate = xFctrl.Cvrate();
        string sTem = string.Empty;
        string sWeight = string.Empty;
        string sOpti = string.Empty;
        string sOnchange = string.Empty;
        string sCk_speci = string.Empty;
        string sXalert = string.Empty;
        string sOverDisplay = string.Empty;
        string sDescDisplay = string.Empty;
        string sLoca = string.Empty;
        bool bCprice = false;

        double nWas = 0;
        double nPrice = 0;
        double nSave = 0;
        double nShip = 0;

        int nId = Convert.ToInt16(Request.QueryString["uid"]);
        if (nId > 0)
        {
            try
            {
                DataTable Dtab = new DataTable();
                Dtab = Tc.ConnDate("select id,title,was,price,preview_pic,overview,details,cate_disc from " + TableName.db_product + " where id=" + nId, 0);

                DataTable DtabM = new DataTable();
                DtabM = Tc.ConnDate("select n.id, n.proid, m.cateid, m.pitem, n.pvalue, n.isuser from " + TableName.db_proxpitem + " as m, " + TableName.db_proxpvalue + " as n where n.proid=" + nId + " and m.id=n.itemid order by nord desc", 0);

                if (DtabM.Rows.Count != 0)
                {
                    sWeight = DtabM.Select("pitem='Weight'")[0]["pvalue"].ToString();
                    sWeight = "<li class=\"i3\">Weight: " + sWeight + "kg</li>";

                    string sDiff = string.Empty;
                    double nDiff = 0;

                    foreach (DataRow Drow in DtabM.Select("isuser=1"))
                    {
                        string[] arrSele = Drow["pvalue"].ToString().Split('₪');
                        if (Drow["pvalue"].ToString().IndexOf("|") != -1)
                        {
                            sOnchange = " onchange=\"up_price(this.value,0);\"";
                            bCprice = true;
                        }
                        else
                        {
                            sOnchange = string.Empty;
                        }
                        sOpti = sOpti + "<li class=\"ix\">\n<span class=\"sele_lab\">" + Drow["pitem"].ToString() + ":</span>\n<select name=\"x" + Drow["pitem"].ToString().ToLower() + "\" id=\"x" + Drow["pitem"].ToString().ToLower() + "\"" + sOnchange + ">\n<option value=\"\">-select-</option>\n";
                        for (int i = 0; i < arrSele.Length; i++)
                        {
                            if (arrSele[i].IndexOf("|") != -1)
                            {
                                sDiff = arrSele[i].Substring(arrSele[i].IndexOf("|") + 1);
                                nDiff = Math.Round(Convert.ToDouble(sDiff) / Convert.ToDouble(aRate[0]), 2);
                                sDiff = arrSele[i].Substring(0, arrSele[i].IndexOf("|"));
                                sOpti = sOpti + "<option value=\"" + arrSele[i] + "+" + nDiff.ToString() + "\">" + sDiff + "</option>\n";
                            }
                            else
                            {
                                if (sOnchange != string.Empty)
                                {
                                    sOpti = sOpti + "<option value=\"" + arrSele[i] + "+0\">" + arrSele[i] + "</option>\n";
                                }
                                else
                                {
                                    sOpti = sOpti + "<option value=\"" + arrSele[i] + "\">" + arrSele[i] + "</option>\n";
                                }
                            }
                        }
                        sOpti = sOpti + "</select>\n</li>\n";
                    }
                }


                switch (Dtab.Rows[0]["cate_disc"].ToString())
                {
                    case "1-1-1":
                        sLoca = "Android 2.x Tablet";
                        break;
                    case "1-1-1-1":
                        sLoca = "Android 2.1 Tablet";
                        break;
                    case "1-1-1-2":
                        sLoca = "Android 2.2 Tablet";
                        break;
                    case "1-1-1-3":
                        sLoca = "Android 2.3 Tablet";
                        break;
                    case "1-1-2":
                        sLoca = "Android 4.x Tablet";
                        break;
                    case "1-1-2-1":
                        sLoca = "Android 4.0 Tablet";
                        break;
                    default:
                        break;
                }

                Ft_ProSort.Text = sLoca;
                Ft_StockUnit.Text = Dtab.Rows[0]["id"].ToString().PadLeft(6, '0');
                Ft_PageTitle.Text = Dtab.Rows[0]["title"].ToString();
                Ft_ProTi.Text = Dtab.Rows[0]["title"].ToString();

                sTem = Dtab.Rows[0]["preview_pic"].ToString().Replace("</center>", "#");
                Ft_PviewPic.Text = sTem.Split('#')[0].ToString().Replace("<center><img", "<img width=\"320\" height=\"268\"").Replace("alt=\"\"", "alt=\""+Dtab.Rows[0]["title"].ToString()+"\"");
                Ft_PviewPics.Text = Dtab.Rows[0]["preview_pic"].ToString().Replace("center>", "span>").Replace("<img", "<img width=\"60\" height=\"50\"");

                nWas = Convert.ToDouble(Dtab.Rows[0]["was"]) / Convert.ToDouble(aRate[0]);
                nWas = Math.Round(nWas, 2);
                Ft_ProWas.Text = aRate[1] + nWas.ToString();

                nPrice = Convert.ToDouble(Dtab.Rows[0]["price"]) / Convert.ToDouble(aRate[0]);
                nPrice = Math.Round(nPrice, 2);

                nSave = nWas - nPrice;

                Ft_ProPrice.Text = aRate[1] + nPrice.ToString();

                Ft_ProSave.Text = aRate[1] + Math.Round(nSave, 2).ToString();

                Ft_ProWeight.Text = sWeight;

                nShip = Math.Round(Si.shopcost / Convert.ToDouble(aRate[0]), 0);
                Ft_ProShipCost.Text = aRate[1] + nShip.ToString();

                Ft_ProTotal.Text = aRate[1] + ( nPrice + Math.Round(Si.shopcost / Convert.ToDouble(aRate[0]), 0) ).ToString();

                if (bCprice)
                {
                    sCk_speci = " onmouseout=\"add2cart(0,0)\" onmouseover=\"add2cart(0,0)\"";
                    sXalert = "\n<div class=\"xalert\" id=\"xalert\" style=\"display: none;\">\n<div class=\"tips\" id=\"xalert_tips\">Please Select:</div>\n<div class=\"txt\" id=\"xalert_value\"></div>\n<i class=\"pointl\"></i><i class=\"angletl\"></i><i class=\"angletr\"></i><i class=\"anglebl\"></i><i class=\"anglebr\"></i>\n</div>";
                }

                Ft_AddToCart.Text = "<a class=\"addtocart\" href=\"javascript:add2cart(" + nId + ",0);\"" + sCk_speci + "><b>Add to Cart</b></a>"+sXalert;
                Ft_AddToWish.Text = "<a href=\"javascript:add2wish(" + nId + ")\">Add to Wishlist</a>";

                Ft_SeleOpti.Text = sOpti;

                for (int i = 0; i < 10; i++)
                {
                    ListItem ltItem = new ListItem();
                    quantity.Items.Add(ltItem);
                    quantity.Items[i].Value = (i + 1).ToString();
                }
				
				Ft_BuyNow.Text = "<a href=\"javascript:add2cart(" + nId + ",1);\"><b>Buy Now</b></a>";
				
                if (bCprice)
                {
                    Ft_Hidden.Text = "<input type=\"hidden\" id=\"hid_was\" name=\"hid_was\" value=\"" + nWas.ToString() + "\" /><input type=\"hidden\" id=\"hid_price\" name=\"hid_price\" value=\"" + nPrice.ToString() + "\" /><input type=\"hidden\" id=\"hid_ship\" name=\"hid_ship\" value=\"" + nShip.ToString() + "\" /><input type=\"hidden\" id=\"hid_rsign\" name=\"hid_rsign\" value=\"" + aRate[1] + "\" />";
                }
                else
                {
                    Ft_Hidden.Text = "<input type=\"hidden\" id=\"hid_ship\" name=\"hid_ship\" value=\"" + nShip.ToString() + "\" /><input type=\"hidden\" id=\"hid_rsign\" name=\"hid_rsign\" value=\"" + aRate[1] + "\" />";
                }
                if (Dtab.Rows[0]["overview"].ToString() == "<br>" || Dtab.Rows[0]["overview"].ToString() == string.Empty)
                {
                    Ft_Tabs.Text = "<a hidefocus=\"true\" href=\"javascript:void(0);\" onclick=\"showtab(1);\" class=\"cur\">Description</a><a hidefocus=\"true\" href=\"javascript:void(0);\" onclick=\"showtab(2);\" class=\"nor\">Feedback</a>";
                    sOverDisplay = "none";
                    sDescDisplay = "block";
                }
                else
                {
                    Ft_Tabs.Text = "<a hidefocus=\"true\" href=\"javascript:void(0);\" onclick=\"showtab(0);\" class=\"cur\">Overview</a><a hidefocus=\"true\" href=\"javascript:void(0);\" onclick=\"showtab(1);\" class=\"nor\">Description</a><a hidefocus=\"true\" href=\"javascript:void(0);\" onclick=\"showtab(2);\" class=\"nor\">Feedback</a>";
                    sOverDisplay = "block";
                    sDescDisplay = "none";
                }
                Ft_ProOverview.Text = "<div class=\"mpage\" id=\"mpage01\" style=\"display: " + sOverDisplay + ";\">" + Dtab.Rows[0]["overview"].ToString() + "</div>";
                Ft_ProDescript.Text = "<div class=\"mpage\" id=\"mpage02\" style=\"display: " + sDescDisplay + ";\">" + Dtab.Rows[0]["details"].ToString() + "</div>";
            }
            catch
            {
                Other.net_alert("Parameter error!", Si.siteurl);
            }
        }

        string sData = string.Empty;

        sData = string.Empty;
        Result = xGList.get_RecList(TableName.db_product, 1, 5, "1=1 order by newid() desc", new string[] { "id", "static", "title", "was", "price", "small_pic" });
        foreach (string[] aList in Result)
        {

            nPrice = Convert.ToDouble(aList[4]) / Convert.ToDouble(aRate[0]);
            nPrice = Math.Round(nPrice, 2);
            aList[4] = aRate[1] + nPrice.ToString();

            sData = sData + String.Format("<ul><li class=\"i1\"><a href=\"{1}.html\"><img src=\"{5}\" width=\"120\" height=\"120\" alt=\"\" /></a></li><li class=\"i2\"><a href=\"{1}.html\" title=\"{2}\">{2}</a></li><li class=\"i3\">{4}</li></ul>", aList) + "\n";
        }
        Ft_ProRand.Text = sData;
        Tc.CloseConn();
    }
}
