﻿String.prototype.trim = function(){	return this.replace(/(^[\s　]*)|([\s　]*$)/g, ""); };
String.prototype.delrn = function(){ return this.replace(/\r\n|\n/gi,""); };
String.prototype.replaceall = function(a,b){ return this.replace(new RegExp(a, "gm"),b); };

function shownavs(way){
    if(this.find("div.nav-sub:0")){
        if(way==0){
            this.find("a.nor:0").className="act";
	        this.find("div.nav-sub:0").style.display="block";
	    }
	    if(way==1){
	        this.find("a.act:0").className="nor";
	        this.find("div.nav-sub:0").style.display="none";
	    }
	}
}

var isearch_lock=false;
function isearch(keys){
    if(keys==null || keys==""){
        window.setTimeout(function(){$3("#tips-pop").style.display="none";$3("#tips-wait").style.display="none";},200);
        return null;
    }
    var recall=function(param){
        if(param.trim()==""){
            $3("#tips-pop").style.display="none";
            window.setTimeout(function(){$3("#tips-wait").style.display="none";},500);
            return null;
        }
        $3("#tips-pop").innerHTML="<ul>"+param+"</ul>";
        $3("#tips-pop").style.display="block";
        window.setTimeout(function(){$3("#tips-wait").style.display="none";},500);
    };
    window.setTimeout(function(){
        if(keys.trim()==$3("#keys-input").value.trim())
        {
            $3("#tips-wait").style.display="block";
            $3.ajax("/aider/update.aspx", "act=isearch&stand=" + keys + "&rnd=" + parseInt(Math.random() * 1000), "html", recall);    
        }
        //var myDate=new Date();
        //$3("#obj_r_01").innerHTML=myDate.getSeconds().toString();
    },1000);
}

$3.onload(function(){
	$3("#mainnav div.nav-ti").each(function(){
		var elem=$3(this).parent("li.lit");
		if($3.isie){    
			$3(this).attachEvent("onmouseover",function(){ shownavs.call(elem,0) });
		}else{
			$3(this).addEventListener("mouseover",function(){ shownavs.call(elem,0) });
		}
		if($3.isie){    
			$3(this).attachEvent("onmouseout",function(){ shownavs.call(elem,1) });
		}else{
			$3(this).addEventListener("mouseout",function(){ shownavs.call(elem,1) });
		}
	});
	
	$3("#entry_elem01 span.selerate i").each(function(){
		$3(this).click(function(){
		    $3.ajax("/aider/update.aspx", "act=currency&stand=" + $3(this).className + "&rnd=" + parseInt(Math.random() * 1000), "html", function(param){window.setTimeout(function(){window.location.reload()},500)} );
		});
	});
	
	var entry_elem = $3("#entry_elem01 span.currbtn:0");
	
	if($3.isie){
	    entry_elem.attachEvent("onmouseover",function(){ entry_elem.style.backgroundPosition="left top"; $3("#entry_elem01 span.selerate:0").style.display="block"; });
	}else{
	    entry_elem.addEventListener("mouseover",function(){ this.style.backgroundPosition="left top"; $3("#entry_elem01 span.selerate:0").style.display="block"; });
	}
	
	if($3.isie){
	    entry_elem.attachEvent("onmouseout",function(){ entry_elem.style.backgroundPosition="left 100px"; $3("#entry_elem01 span.selerate:0").style.display="none"; });
	}else{
	    entry_elem.addEventListener("mouseout",function(){ this.style.backgroundPosition="left 100px"; $3("#entry_elem01 span.selerate:0").style.display="none"; });
	}
});